# Chapter 4
# 4.8 First Common Ancestor
# Question page 110;
# Hints# 10, 16, 28, 36, 46, 70, 80, 96
# Solution page 257

# Q: Design an algorithm and write code to find the first common ancestor of two nodes in a binary tree. Avoid
# storing additional nodes in a data structure. NOTE: This is not necessarily a binary search tree.

# Standard library imports
from enum import Enum

class BTNode_Parent_Link():
	"""Represents a Binary Tree Node object with links to parents."""

	# Setting slots to prevent assigning additional attributes
	__slots__ = "payload", "parent", "left_child", "right_child"

	def __init__(self, payload=None):
		self.payload = payload
		self.parent = None
		self.left_child = None
		self.right_child = None

	# Mutator/setter methods
	def set_payload(self, payload):
		self.payload = payload
	def set_parent(self, parent):
		self.parent = parent
	def set_left_child(self, child):
		self.left_child = child
	def set_right_child(self, child):
		self.right_child = child

	# Accessor/getter methods
	def get_payload(self):
		return self.payload
	def get_parent(self):
		return self.parent
	def get_left_child(self):
		return self.left_child
	def get_right_child(self):
		return self.right_child


class BTNode_Visit_Status():
	"""Represents a Binary Tree Node object with visit status."""
	# NOTE: visit status is determined by boolean value

	# Setting slots to prevent assigning additional attributes
	__slots__ = "payload", "visit_status", "left_child", "right_child"

	def __init__(self, payload=None):
		self.payload = payload
		self.visit_status = False
		self.left_child = None
		self.right_child = None

	# Mutators/setter methods
	def set_payload(self, payload):
		self.payload = payload
	def set_visit_status(self, status):
		self.visit_status = status
	def set_left_child(self, child):
		self.left_child = child
	def set_right_child(self, child):
		self.right_child = child

	# Accessor/getter methods
	def get_payload(self):
		return self.payload
	def get_visit_status(self):
		return self.visit_status
	def get_left_child(self):
		return self.left_child
	def get_right_child(self):
		return self.right_child



class BinaryTree():
	"""Represents a Binary Tree object."""
	# Note: not necessarily a Binary Search Tree

	def __init__(self):
		self.root = None

	def add_root(self, node):
		self.root = node


	def first_common_ancestor_mine_one(self, node_one, node_two):
		"""Determines the first common ancestor of two nodes."""
		# We start with each node to traverse upward and find how many nodes upwards is the root located.
		# If the root is further up from one node compared to the other one, then the we will traverse up the node with
		# the longer path for the distance of the difference of the two paths.
		# Now, we will traverse both nodes simultaneously and compare to check if each preceding parents are the same.

		node_one_path = 0
		parent = node_one

		while parent is not self.root:
			parent = parent.get_parent()
			node_one_path += 1

		node_two_path = 0
		parent = node_two

		while parent is not self.root:
			parent = parent.get_parent()
			node_two_path += 1

		distance = -1
		output = None

		if node_one_path > node_two_path:
			distance = node_one_path - node_two_path

			counter = 0
			cursor_one = node_one
			while counter < distance:
				cursor_one = cursor_one.get_parent()
				counter += 1

			cursor_two = node_two

			output = self.first_common_ancestor_mine_one_helper(cursor_one, cursor_two)


		elif node_two_path > node_one_path:
			distance = node_two_path - node_one_path

			counter = 0
			cursor_two = node_two
			while counter < distance:
				cursor_two = cursor_two.get_parent()
				counter += 1

			cursor_one = node_one

			output = self.first_common_ancestor_mine_one_helper(cursor_one, cursor_two)

		else:
			# equal
			distance = 0

			output = self.first_common_ancestor_mine_one_helper(node_one, node_two)

		return output.get_payload()

	def first_common_ancestor_mine_one_helper(self, node_one, node_two):
		"""Simultaneously traverses upwards from the two nodes and compares until there is a matching parent."""

		cursor_one = node_one
		cursor_two = node_two

		while cursor_one is not cursor_two: # keep running until there is a match
			cursor_one = cursor_one.get_parent()
			cursor_two = cursor_two.get_parent()

			if not cursor_one:
				print("something went wrong, cursor_one is None")
			if not cursor_two:
				print("something went wrong, cursor_two is None")

		return cursor_one


	def first_common_ancestor_official_one(self, node_p, node_q):
		"""Similar to personal solution.
		This approach assumes that nodes have links/pointers to their parents. Find the depth of of each of the two
		nodes. Based on the difference of their depths, traverse upwards from the node for that the distance amount.
		Finally, traverse both nodes until their parents match.
		Note: takes O(d) time where d is the depth of the deeper node."""
		
		delta = self.depth(node_p) - self.depth(node_q) # get difference in depths
		first = node_q if delta > 0 else node_p # get shallower node
		second = node_p if delta > 0 else node_q # get deeper node
		second = self.goUpBy(second, abs(delta)) # move deeper node up

		# Find where paths intersect
		while first and second and (first is not second):
			first = first.get_parent()
			second = second.get_parent()

		return null if (not first or not second) else first.get_payload()

	def goUpBy(self, node, delta):
		while node and delta > 0:
			node = node.get_parent()
			delta -= 1

		return node

	def depth(self, node):
		depth = 0
		while node:
			node = node.get_parent()
			depth += 1
		return depth


	def first_common_ancestor_official_two(self, node_p, node_q):
		"""Thie approach will go up the path from one of the nodes, and will check if the parent's other child,
		aka the sibling of the node covers the other given node. It will keep going up and continue to perform
		that for each new parent and the newly discovered subtree from the sibling.
		Note: O(t) runtime complexity for the size t subtree of the first common ancestor. worse-case O(n) with
		n nodes in the overall tree."""
		if not self.covers(self.root, node_p) or not self.covers(self.root, node_q):
			return None
		elif self.covers(node_p, node_q):
			return node_p.get_payload()
		elif self.covers(node_q, node_p):
			return node_q.get_payload()

		sibling = self.get_sibling(node_p)
		parent = node_p.get_parent()
		while not self.covers(sibling, node_q):
			sibling = self.get_sibling(parent)
			parent = parent.get_parent()

		return parent.get_payload()


	def covers(self, root, node_p):
		if not root:
			return False
		if root is node_p:
			return True
		return self.covers(root.get_left_child(), node_p) or self.covers(root.get_right_child(), node_p)

	def get_sibling(self, node):
		if not node or not node.get_parent():
			return None

		parent = node.get_parent()
		return parent.get_right_child() if (parent.get_left_child() is node) else parent.get_left_child()



	def first_common_ancestor_official_three(self, node_p, node_q):
		"""This approach considers the two nodes without any links to parents."""
		if (not self.covers(self.root, node_p)) or (not self.covers(self.root, node_q)):
			return None

		return self.ancestor_helper(self.root, node_p, node_q)

	def ancestor_helper(self, root, node_p, node_q):
		if (not root)  or (root is node_p) or (root is node_q):
			return root.get_payload()

		p_is_on_left = self.covers(root.get_left_child(), node_p)
		q_is_on_left = self.covers(root.get_left_child(), node_q)
		if p_is_on_left != q_is_on_left: # nodes are on different side
			return root.get_payload()

		child_side = root.get_left_child() if p_is_on_left else root.get_right_child()
		return self.ancestor_helper(child_side, node_p, node_q)



if __name__ == "__main__":


	print("\nFirst Common Ancestor")

	root = BTNode_Parent_Link(10)

	node_20 = BTNode_Parent_Link(20)
	root.set_left_child(node_20)
	node_20.set_parent(root)

	node_2 = BTNode_Parent_Link(2)
	root.set_right_child(node_2)
	node_2.set_parent(root)

	node_30 = BTNode_Parent_Link(30)
	node_20.set_left_child(node_30)
	node_30.set_parent(node_20)

	node_40 = BTNode_Parent_Link(40)
	node_20.set_right_child(node_40)
	node_40.set_parent(node_20)

	node_9 = BTNode_Parent_Link(9)
	node_30.set_right_child(node_9)
	node_9.set_parent(node_30)

	node_6 = BTNode_Parent_Link(6)
	node_40.set_left_child(node_6)
	node_6.set_parent(node_40)

	b_tree = BinaryTree()
	b_tree.add_root(root)


	print()
	print("Personal solution")
	print(b_tree.first_common_ancestor_mine_one(node_9, node_6))

	print()
	print(b_tree.first_common_ancestor_mine_one(node_40, node_9))

	print()
	print(b_tree.first_common_ancestor_mine_one(node_6, node_2))


	print()
	print("Official solution one")
	print(b_tree.first_common_ancestor_official_one(node_9, node_6))

	print()
	print(b_tree.first_common_ancestor_official_one(node_40, node_9))

	print()
	print(b_tree.first_common_ancestor_official_one(node_6, node_2))

	print()
	print("Official solution two")
	print(b_tree.first_common_ancestor_official_two(node_9, node_6))

	print()
	print(b_tree.first_common_ancestor_official_two(node_40, node_9))

	print()
	print(b_tree.first_common_ancestor_official_two(node_6, node_2))

	print()
	print("Official solution three")
	print(b_tree.first_common_ancestor_official_three(node_9, node_6))

	print()
	print(b_tree.first_common_ancestor_official_three(node_40, node_9))

	print()
	print(b_tree.first_common_ancestor_official_three(node_6, node_2))


	print()








