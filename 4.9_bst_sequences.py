# Chapter 4
# 4.9 BST Sequences
# Question page 110;
# Hints# 39, 48, 66, 82
# Solution page 262

# Q: A binary search tree was created by traversing through an array from left
# to right and inserting each element. Given a binary search tree with distinct
# elements, print all possible arrays that could have led to this tree.

